package net.tyan.cloudserver.utils;

import net.tyan.cloudapi.reference.Reference;

import java.io.IOException;

/**
 * Created by Kevin
 */

public class TeamSpeak {

    public static void restart() {
        try {
            ProcessBuilder pb = new ProcessBuilder(Reference.TEAMSPEAK_SCRIPT, "restart");
            Process process = pb.start();
            System.out.println("TeamSpeak successfully restarted!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
