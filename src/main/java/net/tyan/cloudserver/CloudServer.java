package net.tyan.cloudserver;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.concurrent.GlobalEventExecutor;
import net.tyan.cloudapi.reference.Reference;
import net.tyan.cloudserver.networking.ServerChannelHandler;


public class CloudServer {

	public static void main(String[] args) {
		new CloudServer();
	}

    static NioEventLoopGroup parent = new NioEventLoopGroup();
    static NioEventLoopGroup child = new NioEventLoopGroup();
    public static ChannelGroup allChannels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

	public CloudServer() {
		try {
			ServerBootstrap bootstrap = new ServerBootstrap()
					.group(parent, child)
					.channel(NioServerSocketChannel.class)
					.option(ChannelOption.SO_BACKLOG, 100)
					.childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            pipeline.addLast(new ServerChannelHandler());
                        }
                    });
			Channel serverChannel = bootstrap.bind(Reference.PORT).sync().channel().closeFuture().sync().channel();
            allChannels.add(serverChannel);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			parent.shutdownGracefully();
			child.shutdownGracefully();
		}
	}

    public static void shutdown() {
        ByteBuf buf = Unpooled.buffer();
        buf.writeInt(0);
        for (Channel ch : allChannels) {
            ch.writeAndFlush(buf);
        }

        parent.shutdownGracefully();
        child.shutdownGracefully();
    }
}